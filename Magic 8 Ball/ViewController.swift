//
//  ViewController.swift
//  Magic 8 Ball
//
//  Created by Hasan Tagiyev on 8/5/18.
//  Copyright © 2018 Hasan Tagiyev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var ballImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        switchBall()
    }

    func switchBall() -> Void {
        let randomBallIndex = arc4random_uniform(5)
        let ballName = "ball" + String(randomBallIndex+1)
        ballImage.image = UIImage(named: ballName)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func onAsk(_ sender: UIButton) {
        self.switchBall()
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        self.switchBall()
    }
    
}

